import random
from string import punctuation
import speech_recognition as sr
from gtts import gTTS
from playsound import playsound

global begin_db
global responce_db

begin_db = ["hi", "hey"]
responce_db = {
				"hi": ["how are you", "hi"],
				"hey": ["how are you", "hey"],
			 	"how are you": ["good", "okay", "great",],
			 	"khjhaskjdhjkhenkajjhuy3jh42bjknuc": ["Sorry, i didnt catch that, could you say it again?"]
			 	}

DEBUGGING = False
VOICEINPUT = False
VOICEOUTPUT = False


BOTNAME = "Bot"

def rempunc(s):

	return ''.join(c for c in s if c not in punctuation).lower()

def SpeachRec():
	while 1:
		r = sr.Recognizer() # defines speech recog
		with sr.Microphone() as source: # makes the source be the mic (CAN BE CHANGED)
			print("...") # This is the prompt. I probably should make this tts... (CAN BE CHANGED)
			uinput = r.listen(source) # the input method, like var = input("Test: ")

			try:
				uinput = r.recognize_google(uinput) # this uses the google api, you can change it, but google is the best for macs
				print("User: " + uinput)
				return uinput # This is just there so you can check to make sure that your speech was caught correctly.

			except sr.UnknownValueError:
				print("error, try again")

def sinput():
	if VOICEINPUT:
		return SpeachRec()
	else:
		return rempunc(input("User: "))

def TextToSpeach(output):
	tts = gTTS(text=output, lang='en',) # creates the variable to make a sound file of the so called reply
	tts.save("reply.mp3") # actual saving of the speech
	playsound('reply.mp3') # finally using playsound, you now can play the sound file.

def sout(reply):
	if VOICEOUTPUT:
		TextToSpeach(reply)
	print(BOTNAME + ": " + reply)

def debugging(dbstr):
	if DEBUGGING:
		print("DEBUGGING:")
		print(dbstr)

def main():

	# Variable used to exit the loop
	exitval = False

	while exitval != True:

		if random.choice([True, False]):
	
			res = random.choice(begin_db)
			wm = True
	
		else: 
	
			inp = sinput()
	
			if inp == 'exit':

				debugging("exit message seen")
				wm = False
				exitval = True
	
			else:
	
				try:
					res = random.choice(responce_db[inp])
					debugging("Found responce")
					wm = True
	
				except:
					print("No responce data found, a new session will begin to gather more data...")
					debugging("No responce found")
					print("")
					begin_db.append(inp)
					wm = False
	
		while wm:
	
			sout(res)
	
			inp = sinput()
			if inp == 'exit':
				wm = False
				exitval = True
	
			if wm:
	
				try:
					responce_db[res].append(inp)
					debugging("Added new responce '" + inp + "'' under old key '" + res + "'")
		
				except:
					responce_db[res] = [inp]
					debugging("Added new responce '" + inp + "'' under new key '" + res + "'")
		
				try:
					res = random.choice(responce_db[inp])
					debugging("Found responce")
		
				except:
					print("No responce data found, a new session will begin to gather more data...")
					debugging("No responce found")
					print("")
	
					wm = False


while 1:
	
	inv = input(">>> ")
	
	if inv == "save":
		vname = input("Name of file: ")
		file = open(vname + ".txt", "w")
		file.writelines(["begin_db = " + str(begin_db) + '\n', "responce_db = " + str(responce_db)])
		file.close()
		print("Save Complete")
	elif inv == "load":
		xname = input("Name of file: ")
		try:
			file = open(xname + ".txt", "r")
			for line in file.readlines():
				exec(line)
			file.close()
		except FileNotFoundError:
			print("Error, Load File Not Found")
		
		print("Load Comlpete")
	elif inv == "run":
		main()
	else:
		print("Error, Invalid Command")
		print("Avalible Commands are: 'save', 'load', 'run'")


